package pl.asist.recruitment.WorldWeather;

import com.google.gson.JsonObject;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.asist.recruitment.WorldWeather.Controller.ChooseCityWeatherController;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
public class WorldWeatherApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(WorldWeatherApplication.class, args);


		ChooseCityWeatherController chooseCityWeatherController = new ChooseCityWeatherController();
		JSONObject jsonObject = chooseCityWeatherController.CheckWeatherGet("Gdańsk", "2021-02-15");
		String req = String.valueOf(chooseCityWeatherController.CheckWeatherGet("Gdańsk", "2021-02-15"));
		System.out.println(req);
		String res = chooseCityWeatherController.CheckWeatherPost(jsonObject);
		System.out.println(res);
	}

}
