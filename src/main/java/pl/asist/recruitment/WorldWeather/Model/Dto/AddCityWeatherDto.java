package pl.asist.recruitment.WorldWeather.Model.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddCityWeatherDto {
    private String city;
    private String date;
    private int maxtempC, mintempC;


}
