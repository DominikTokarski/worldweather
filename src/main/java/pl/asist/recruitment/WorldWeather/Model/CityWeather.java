package pl.asist.recruitment.WorldWeather.Model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.asist.recruitment.WorldWeather.Model.Dto.AddCityWeatherDto;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class CityWeather {

 @Id
 @GeneratedValue(strategy = GenerationType.AUTO)
 private Long id;

 private String city;
 private String date;
 private int numberOfInquiries = 0,  maxTemp, avgTemp ,minTemp;

 public static CityWeather addCityWeatherFromDto(AddCityWeatherDto addCityWeatherDto){
  CityWeather cityWeather = new CityWeather();
  cityWeather.setCity(addCityWeatherDto.getCity());
  cityWeather.setDate(addCityWeatherDto.getDate());
  cityWeather.setMaxTemp(addCityWeatherDto.getMaxtempC());
  cityWeather.setMinTemp(addCityWeatherDto.getMintempC());
  cityWeather.setAvgTemp((cityWeather.maxTemp + cityWeather.maxTemp)/2);

  return cityWeather;
 }

}
