package pl.asist.recruitment.WorldWeather.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.asist.recruitment.WorldWeather.Model.CityWeather;
import pl.asist.recruitment.WorldWeather.Model.Dto.AddCityWeatherDto;
import pl.asist.recruitment.WorldWeather.Repository.CityWeatherRepository;

import java.util.Optional;

@Service
public class CityWeatherService {

    @Autowired
    private CityWeatherRepository cityWeatherRepository;


    public Optional<CityWeather> addCityWeather(AddCityWeatherDto addCityWeatherDto) {

            CityWeather cityWeather = CityWeather.addCityWeatherFromDto(addCityWeatherDto);
            cityWeather.setNumberOfInquiries(1);
            cityWeatherRepository.save(cityWeather);
            return Optional.of(cityWeather);

    }
}
