package pl.asist.recruitment.WorldWeather.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.asist.recruitment.WorldWeather.Model.CityWeather;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface CityWeatherRepository extends JpaRepository<CityWeather, Long> {

    Optional<CityWeather> findByDateAndCity(String date, String city);
}
