package pl.asist.recruitment.WorldWeather.Controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import okhttp3.*;
import org.apache.tomcat.util.json.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.asist.recruitment.WorldWeather.Model.CityWeather;
import pl.asist.recruitment.WorldWeather.Model.Dto.AddCityWeatherDto;
import pl.asist.recruitment.WorldWeather.Repository.CityWeatherRepository;
import pl.asist.recruitment.WorldWeather.Service.CityWeatherService;

import java.io.IOException;
import java.util.Optional;

@Controller
public class ChooseCityWeatherController {

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    @Autowired
    private CityWeatherService cityWeatherService;

    @Autowired
    private CityWeatherRepository cityWeatherRepository;

    @GetMapping("/checkWeather")
    public JSONObject CheckWeatherGet (@RequestParam("city") String city, @RequestParam("date") String date) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("city", city);
            jsonObject.put("date", date);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;

    }

        @PostMapping("/checkWeather")
        public String CheckWeatherPost (JSONObject jsonObject){
        String body = jsonObject.toString();
        OkHttpClient client = new OkHttpClient();
        Gson gson = new Gson();
            AddCityWeatherDto addCityWeatherDto = gson.fromJson(body,AddCityWeatherDto.class);
            if (!cityWeatherRepository.findByDateAndCity(addCityWeatherDto.getDate(),addCityWeatherDto.getCity())
                    .isPresent()){

                Optional<CityWeather> cityWeather = cityWeatherService.addCityWeather(addCityWeatherDto);
                String Json = gson.toJson(cityWeather,JsonObject.class);
                Response response = null;
                try {
                    RequestBody requestBody = RequestBody.create(body,JSON);
                    Request request = new Request.Builder()
                            .url("http://api.worldweatheronline.com/premium/v1/weather.ashx?key=49fe70e88ee34f4f884132553211202&format=json")
                            .post(requestBody)
                            .build();
                    response = client.newCall(request).execute();
                    String resStr = response.body().string();
                    return resStr;
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            Optional<CityWeather> optionalCityWeather = cityWeatherRepository
                    .findByDateAndCity(addCityWeatherDto.getDate(),addCityWeatherDto.getCity());
            String cityWeather = gson.toJson(optionalCityWeather);
        return cityWeather;
        }

}
